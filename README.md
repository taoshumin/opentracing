# opentracing 链路追踪

### 基础知识介绍

- trace: 表示一次完整的链路追踪。
- span: 一个独立的工作单元，（一个函数调用）
  - ChildOf: 父span依赖子span.
  - FollowFrom: 父span不依赖子span.
- tags: 用户标签.
- logs: 与tag相似，记录日志.

### Docker启动

```shell
docker run -d --name jaeger \
  -e COLLECTOR_ZIPKIN_HTTP_PORT=9411 \
  -p 5775:5775/udp \
  -p 6831:6831/udp \
  -p 6832:6832/udp \
  -p 5778:5778 \
  -p 16686:16686 \
  -p 14268:14268 \
  -p 9411:9411 \
  jaegertracing/all-in-one:latest
```

- [访问:  http://localhost:16686/search](http://localhost:16686/search)