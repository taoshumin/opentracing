/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	"time"
)

func main() {
	cfg := jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans:          true,
			CollectorEndpoint: "http://localhost:14268/api/traces",
		},
		ServiceName: "测序仪",
	}

	// 一个tracer代表一个完整的链
	tracer, closer, err := cfg.NewTracer()
	if err != nil {
		panic(err)
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	// 执行最小单元
	span := opentracing.StartSpan("Zac-Test")
	defer span.Finish()

	ctx := opentracing.ContextWithSpan(context.Background(), span)
	Example1(ctx)
}

func Example1(ctx context.Context) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Example1")
	defer span.Finish()

	Example2(ctx)
	time.Sleep(3 * time.Second)
}

func Example2(ctx context.Context) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Example2")
	defer span.Finish()

	time.Sleep(1 * time.Second)
	span.LogFields(log.String("event", "error"), log.Error(fmt.Errorf("test error ")))
	span.SetTag("key", "value")
}
