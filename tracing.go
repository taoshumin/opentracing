/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tracing

import (
	"fmt"
	"github.com/influxdata/influxdb/v2/snowflake"
	"github.com/opentracing/opentracing-go"
	jaegerconfig "github.com/uber/jaeger-client-go/config"
	"go.uber.org/zap"
	pzap "test.io/tracing/zap"
)

const (
	// LogTracing enables tracing via zap logs
	LogTracing = "log"
	// JaegerTracing enables tracing via the Jaeger client library
	JaegerTracing = "jaeger"
)

func initTracing(opts string, log *zap.Logger) {
	switch opts {
	case LogTracing:
		opentracing.SetGlobalTracer(pzap.NewTracer(log, snowflake.NewIDGenerator()))
	case JaegerTracing:
		cfg, err := jaegerconfig.FromEnv()
		if err != nil {
			return
		}

		tracer, closer, err := cfg.NewTracer()
		if err != nil {
			return
		}
		fmt.Println(closer) // TODO use closer to close tracer.
		opentracing.SetGlobalTracer(tracer)
	}
}
